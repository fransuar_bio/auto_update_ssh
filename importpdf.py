import sys
import string
import os
from os import listdir

from subprocess import Popen

from datetime import date, datetime
from pathlib import Path
import sys
from proteus import Model
from proteus import config as pconfig
from optparse import OptionParser
from paramiko import SSHClient, AutoAddPolicy


def main(options):
    config = pconfig.set_trytond(options.database, options.admin, config_file=options.config_file)
    Lang = Model.get('ir.lang')
    (es,) = Lang.find([('code', '=', 'es')])
    es.translatable = True
    es.save()

    attach_files(config, es, options)
    print ("done.")

def attach_files(config, es, options):
    Patient = Model.get('gnuhealth.patient')
    Attachment = Model.get('ir.attachment')
    attachment = Attachment()

    #leer un archivo
    ssh = SSHClient()
    #ssh.set_missing_host_key_policy(AutoAddPolicy())
    ssh.load_system_host_keys()

    ssh.connect('url', 22, 'user','pass')
    ruta = 'ORIGIN'

    entrada, salida, error = ssh.exec_command('ls '+ ruta)
    directorios = salida.read().decode('utf8').split('\n')
    directorios = list(filter(None, directorios))
    print(directorios)
    localfilepath = '/tmp'
    for directorio in directorios:
        print("00000000000000",directorio)
        genero = directorio[0]
        ref = directorio[1:].split('.')[0]
        patient= Patient.find([('puid', '=', ref)])
        if patient:
            entrada, salida, error = ssh.exec_command('ls '+ ruta+directorio)
            archivos = salida.read().decode('utf8').split('\n')
            archivos = list(filter(None, archivos))
            for archivo in archivos:
                print(archivo)
                ftp_client = ssh.open_sftp()
                ftp_client.get(ruta+directorio+'/'+archivo,localfilepath+'/'+archivo)
                ftp_client.close()
                with open(localfilepath+'/'+archivo,"rb") as file_data:
                    data = file_data.read()
                    attachment = Attachment()
                    attachment.resource= patient[0]
                    attachment.data = data
                    attachment.name = archivo
                    #find detecta coincidencias y len tira la cantidad de repeticiones
                    similar_file=Attachment.find([('name','=',archivo)]) #devuelve una lista de coincidencias
                    # en el caso de que no se encuentre ningun archivo con el mismo nombre en adjuntos del paciente
                    #lo guarda!
                    print('\n'*2,)
                    if len(similar_file) == 0:
                        attachment.save()
                    # en el caso de que haya coincidencia con otros archivos comparamos el tamaño del archivo.
                        print('\n'*2,attachment.name)
                    if len(similar_file) != 0:
                    #else:
                        #size_file = os.path.getsize(ruta+"/"+contenido)  #me da el tamaño del archivo que se quiere adjuntar en Kb
                        data_file = similar_file[0]
                        data_size = bytes(similar_file[0].data_size )
                        print("aca estoy 000000000000",attachment.name)
                        #si el archivo a adjuntar es mayor en relacion al que ya esta adjuntado, lo actualiza
                        if data_size > attachment.data:
                            print('es mayor')
                            data_file.name = archivo
                            data_file.data = attachment.data
                            data_file.save()
                Popen(['rm',localfilepath+'/'+archivo])
    ssh.close()

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--database', dest='database')
    parser.add_option('-c', '--config-file', dest='config_file')
    parser.add_option('-a', '--admin', dest='admin')
    parser.set_defaults(user='admin')

    options, args = parser.parse_args()
    if args:
        parser.error('Parametros incorrectos')
    if not options.database:
        parser.error('Se debe definir [nombre] de base de datos')
    if not options.config_file:
        parser.error('Se debe definir el path absoluto al archivo de '
            'configuracion de trytond')
    if not options.admin:
        parser.error('Se debe definir el usuario admin')

    main(options)
